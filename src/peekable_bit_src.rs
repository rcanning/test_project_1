use super::bit_src::BitSrc;

/// # PeekableBitSrc
/// Adds to BitSrc the ability to peek up to 64 bits, without consuming them.
pub trait PeekableBitSrc: BitSrc {
    /// Same as extract(), except no bits are consumed.
    fn peek(&mut self, n: u8) -> Option<u64>;
}

use crate::defs::BITS_PER_BYTE;

// ================================================================================================
/// Implements PeekableBitSrc from any BitSrc, by caching the most recently
/// extracted 64 bits.
// ================================================================================================
pub struct Peekable<S>
where
    S: BitSrc,
{
    src: S,

    // Cached bits. If < 64 bits in cache, the data bits are shifted to the
    // right, and the left-most bits are zero.
    cache: u64,
    num_bits_in_cache: u8,
}

/// Create a PeekableBitSrc from any BitSrc.
pub fn peekable<S>(src: S) -> Peekable<S>
where
    S: BitSrc,
{
    Peekable {
        src: src,
        cache: 0,
        num_bits_in_cache: 0,
    }
}

impl<S> BitSrc for Peekable<S>
where
    S: BitSrc,
{
    fn extract(&mut self, n: u8) -> Option<u64> {
        match self.peek(n) {
            None => None,
            Some(bits) => {
                self.clear_from_cache(n);
                Some(bits)
            }
        }
    }

    fn num_unaligned_bits(&self) -> u8 {
        (self.src.num_unaligned_bits() + self.num_bits_in_cache) % BITS_PER_BYTE
    }
}

impl<S> PeekableBitSrc for Peekable<S>
where
    S: BitSrc,
{
    fn peek(&mut self, n: u8) -> Option<u64> {
        if self.fill(n) {
            Some(self.cache >> (self.num_bits_in_cache - n))
        } else {
            None
        }
    }
}

impl<S> Peekable<S>
where
    S: BitSrc,
{
    // Removes the next 'n' bits from the cache.
    fn clear_from_cache(&mut self, n: u8) {
        // Note that clear_bits_64() numbers bits from 0 (right-most), to 63 (left-most).
        use super::bits::clear_bits_64;
        clear_bits_64(
            &mut self.cache,
            (self.num_bits_in_cache - n) as usize,
            n as usize,
        );
        self.num_bits_in_cache -= n;
    }

    // Gets 'n' bits into the cache. Returns false if source ran out of bits.
    fn fill(&mut self, n: u8) -> bool {
        if self.num_bits_in_cache >= n {
            return true;
        }
        let bits_needed = n - self.num_bits_in_cache;
        match self.src.extract(bits_needed) {
            None => return false,
            Some(b) => {
                self.cache <<= bits_needed;
                self.cache |= b;
                self.num_bits_in_cache = n;
            }
        }
        true
    }
}

#[cfg(test)]
mod tests {
    use super::super::SliceBitSrc;
    use super::*;

    #[test]
    fn test_bit_by_bit() {
        let bytes = [0xAE, 0xAA, 0xA0, 0xAA];
        let ex = SliceBitSrc::new(&bytes[..], 0);
        let mut ex = peekable(ex);
        assert_eq!(ex.peek(8).unwrap(), 0xAE);
        assert_eq!(ex.peek(16).unwrap(), 0xAEAA);
        ex.extract(3);
        assert_eq!(ex.peek(8).unwrap(), 0x75);
        assert_eq!(ex.peek(16).unwrap(), 0x7555);
    }
}
