use crate::bit_src::BitSrc;

/// # CountingBitSrc
/// A BitSrc adaptor that counts the bits it has extracted.
/// # Examples
/// ```
/// use bits::{CountingBitSrc, BitSrc, SliceBitSrc};
/// let bytes = [0xAE, 0x0A]; // 1010 1110 0000 1010
/// let mut src = CountingBitSrc::new(SliceBitSrc::new(&bytes, 0));
/// assert_eq!(src.num_extracted_bits(), 0);
/// src.extract_bit().unwrap();
/// assert_eq!(src.num_extracted_bits(), 1);
/// src.extract(12).unwrap();
/// assert_eq!(src.num_extracted_bits(), 13);
/// ```

pub struct CountingBitSrc<S>
where
    S: BitSrc,
{
    src: S,
    bit_count: usize,
}

impl<S> CountingBitSrc<S>
where
    S: BitSrc,
{
    pub fn new(src: S) -> CountingBitSrc<S> {
        CountingBitSrc {
            src: src,
            bit_count: 0,
        }
    }
}

impl<S> BitSrc for CountingBitSrc<S>
where
    S: BitSrc,
{
    fn extract(&mut self, n: u8) -> Option<u64> {
        match self.src.extract(n) {
            None => None,
            Some(b) => {
                self.bit_count += n as usize;
                Some(b)
            }
        }
    }

    fn num_unaligned_bits(&self) -> u8 {
        self.src.num_unaligned_bits()
    }
}

impl<S> CountingBitSrc<S>
where
    S: BitSrc,
{
    pub fn num_extracted_bits(&self) -> usize {
        self.bit_count
    }
}
