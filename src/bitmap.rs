use std::collections::VecDeque;

use std::cmp;

use std::fmt;

use super::bits;

/// # Bitmap
///
/// Fixed-size set of bit flags. Can be efficiently shifted in either direction.
///
/// Bits are numbered from the left, so the left-most bit is 0, the next bit 1, etc. Note
/// that this is the opposite of how the bits in an integer are traditionally numbered - it
/// can get confusing when mixing the two.
///
/// TODO: make resizable.
pub struct Bitmap {
    // Stores the bits.
    bits: VecDeque<u64>,

    // Number of bits in the Bitmap.
    size: usize,

    // Position of the first actual bit in bits[0]. 0 means all the bits in bits[0] are
    // used (unless size < bits_per_word); 1 means the first bit is unused and the rest
    // are used, and so on. Unused bits are undefined.
    first_offset: usize,

    // Number of bits in each u64 to use. Normally 64, but can be lowered for testing. This
    // would be better as a compile-time constant - maybe when integral generic parameters
    // are added to Rust.
    bits_per_word: usize,
}

/// Iterates the bits of a Bitmap from left (bit 0) to right (bit n-1).
pub struct BitmapIter<'a> {
    b: &'a Bitmap,
    i: usize,
}

impl<'a> Iterator for BitmapIter<'a> {
    type Item = bool;

    fn next(&mut self) -> Option<bool> {
        if self.i == self.b.len() {
            None
        } else {
            self.i += 1;
            Some(self.b.is_set(self.i - 1))
        }
    }
}

impl Bitmap {
    /// Creates a new bitmap of the specified size. All bits are initially zero.
    pub fn new(size: usize) -> Bitmap {
        Bitmap::init(size, 64)
    }

    fn init(size: usize, bits_per_word: usize) -> Bitmap {
        assert!(size > 0);

        // The required size of the VecDeque is basically (size / bits_per_word) + 1, but we
        // may need an extra word for when the bits start from the middle of a word - e.g. if
        // size==100, 2 64-bit words will normally be enough, but sometimes you'll need 3 if
        // the bits start from position 60 (say) in the first word.
        let mut v = VecDeque::with_capacity((size / bits_per_word) + 2);
        let cap = v.capacity();
        v.resize(cap, 0);

        Bitmap {
            bits: v,
            size: size,
            first_offset: 0,
            bits_per_word: bits_per_word,
        }
    }

    pub fn iter<'a>(&'a self) -> BitmapIter<'a> {
        BitmapIter { b: self, i: 0 }
    }

    /// Sets the range of bits [start, start+n). Panics if start+n is out of range.
    pub fn set_range(&mut self, start: usize, n: usize) {
        self.apply_to_range(start, n, bits::set_bits_64)
    }

    /// Clears the range of bits [start, start+n). Panics if start+n is out of range.
    pub fn clear_range(&mut self, start: usize, n: usize) {
        self.apply_to_range(start, n, bits::clear_bits_64)
    }

    /// Sets bit 'i'. Panics if 'i' is out of range.
    pub fn set(&mut self, i: usize) {
        self.set_range(i, 1)
    }

    /// Clears bit 'i'. Panics if 'i' is out of range.
    pub fn clear(&mut self, i: usize) {
        self.clear_range(i, 1)
    }

    /// Panics if 'i' is out of range.
    pub fn is_set(&self, i: usize) -> bool {
        assert!(i < self.len());
        let (bit_i, bit_offset) = self.pos_to_index(i);
        bits::is_set_64(&self.bits[bit_i], bit_offset)
    }

    /// Number of bits (i.e. the 'size' parameter to new()).
    pub fn len(&self) -> usize {
        self.size
    }

    /// Shifts left by 'n' bits. The bits [0, n) are discarded, and the remaining
    /// bits are shifted 'n' places to the left. The 'n' new bits at the right
    /// will all be 0. If 'n' >= the size of the Bitmap, the result will be
    /// all zeroes.
    pub fn shl(&mut self, mut n: usize) {
        while n > 0 {
            let shift_n = cmp::min(n, self.bits_per_word - self.first_offset);
            self.first_offset += shift_n;
            if self.first_offset == self.bits_per_word {
                self.first_offset = 0;
                self.bits.pop_front();
                self.bits.push_back(0);
            }
            n -= shift_n;
        }
    }

    /// Shifts right by 'n' bits. The bits [len()-n, len()) are discarded, and the
    /// remaining bits are shifted 'n' places to the right. The 'n' new bits at
    /// left end are all 0. If 'n' >= the size of the Bitmap, the result will be
    /// all zeroes.
    pub fn shr(&mut self, mut n: usize) {
        while n > 0 {
            let shift_n = cmp::min(n, self.first_offset);
            self.first_offset -= shift_n;

            // Clear all the new bits.
            bits::clear_bits_64(&mut self.bits[0], self.first_offset, shift_n);

            n -= shift_n;
            if self.first_offset == 0 && n > 0 {
                // bits_per_word is not a valid value for first_offset, but the next
                // iteration will put things right.
                self.first_offset = self.bits_per_word;
                self.bits.push_front(0);
                self.bits.pop_back();
            }
        }
    }

    // Applies the operation 'f' to every bit in the specified range.
    fn apply_to_range<F>(&mut self, start: usize, mut n: usize, f: F)
    where
        F: Fn(&mut u64, usize, usize),
    {
        assert!(start + n <= self.size);
        let (mut bit_i, mut bit_offset) = self.pos_to_index(start);
        while n > 0 {
            let bits_to_set = cmp::min(n, self.bits_per_word - bit_offset);
            f(&mut self.bits[bit_i], bit_offset, bits_to_set);
            bit_offset = 0;
            bit_i += 1;
            n -= bits_to_set;
        }
    }

    fn pos_to_index(&self, i: usize) -> (usize, usize) {
        let tmp = i + self.first_offset;
        (tmp / self.bits_per_word, tmp % self.bits_per_word)
    }
}

impl fmt::Display for Bitmap {
    /// Prints the bits from bit 0 upwards
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let mut s = String::new();
        for b in self.iter() {
            s.push_str(if b { "1" } else { "0" });
        }
        write!(fmt, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::super::bits;
    use super::*;

    #[test]
    fn test_shl_wrap_around() {
        // Tests that shl() behaves correctly as the bits wrap around the end of a word.
        for bits_per in 1..9 {
            let mut b = Bitmap::init(5, bits_per);
            b.set(0);
            b.set(2);
            b.set(4);
            assert_eq!(format!("{}", b), "10101");
            b.shl(1);
            assert_eq!(format!("{}", b), "01010");
            b.shl(1);
            assert_eq!(format!("{}", b), "10100");
            b.shl(1);
            assert_eq!(format!("{}", b), "01000");
            b.set(4);
            assert_eq!(format!("{}", b), "01001");
            b.shl(1);
            assert_eq!(format!("{}", b), "10010");
            b.shl(1);
            assert_eq!(format!("{}", b), "00100");
            b.shl(1);
            assert_eq!(format!("{}", b), "01000");
        }
    }

    #[test]
    fn test_shr_wrap_around() {
        // Tests that shr() behaves correctly as the bits wrap around the beginning
        // of a word.
        for bits_per in 1..9 {
            let mut b = Bitmap::init(5, bits_per);
            b.set(0);
            b.set(2);
            b.set(4);
            assert_eq!(format!("{}", b), "10101");
            b.shr(1);
            assert_eq!(format!("{}", b), "01010");
            b.shr(1);
            assert_eq!(format!("{}", b), "00101");
            b.shr(1);
            assert_eq!(format!("{}", b), "00010");
            b.set(1);
            assert_eq!(format!("{}", b), "01010");
            b.shr(1);
            assert_eq!(format!("{}", b), "00101");
            b.shr(1);
            assert_eq!(format!("{}", b), "00010");
            b.shr(1);
            assert_eq!(format!("{}", b), "00001");
        }
    }

    #[test]
    fn test_set_range() {
        for bits_per in 1..11 {
            let mut b = Bitmap::init(10, bits_per);
            b.set_range(3, 4);
            assert_eq!(format!("{}", b), "0001111000");
            b.set_range(5, 3);
            assert_eq!(format!("{}", b), "0001111100");
            b.set_range(0, 1);
            assert_eq!(format!("{}", b), "1001111100");
            b.set_range(1, 9);
            assert_eq!(format!("{}", b), "1111111111");
        }

        let mut b = Bitmap::init(70, 11);
        b.set_range(3, 4);
        assert_eq!(
            format!("{}", b),
            "0001111000000000000000000000000000000000000000000000000000000000000000"
        );
        b.set_range(60, 9);
        assert_eq!(
            format!("{}", b),
            "0001111000000000000000000000000000000000000000000000000000001111111110"
        );
    }

    #[test]
    #[should_panic]
    fn test_set_range_out_of_bounds() {
        let mut b = Bitmap::new(10);
        b.set_range(3, 8);
    }

    #[test]
    fn test_shl() {
        let mut b = Bitmap::init(20, 18);
        b.set_range(13, 4);
        assert_eq!(format!("{}", b), "00000000000001111000");
        b.shl(1);
        assert_eq!(format!("{}", b), "00000000000011110000");
        b.shl(6);
        assert_eq!(format!("{}", b), "00000011110000000000");
        b.shl(1000);
        assert_eq!(format!("{}", b), "00000000000000000000");
    }

    #[test]
    fn test_shr() {
        let mut b = Bitmap::init(20, 1);
        b.set_range(3, 4);
        assert_eq!(format!("{}", b), "00011110000000000000");
        b.shr(1);
        assert_eq!(format!("{}", b), "00001111000000000000");
        b.shr(6);
        assert_eq!(format!("{}", b), "00000000001111000000");
        b.shr(1000);
        assert_eq!(format!("{}", b), "00000000000000000000");
    }

    #[test]
    fn regression_test_1() {
        let mut b = Bitmap::new(10);
        b.set_range(0, 10);
        assert_eq!(format!("{}", b), "1111111111");
        b.shl(3);
        assert_eq!(format!("{}", b), "1111111000");
        b.shr(5);
        assert_eq!(format!("{}", b), "0000011111");
    }

    fn diff(b: &Bitmap, c: &u64) -> Option<usize> {
        for i in 0..64 {
            if b.is_set(i) != bits::is_set_64(c, i) {
                return Some(i);
            }
        }
        None
    }

    #[test]
    fn test_64() {
        // Applies a series of operations to both a Bitmap and a u64, and checks after each
        // operation that they stil match.
        for bits_per in 1..20 {
            let mut b = Bitmap::init(64, bits_per);
            let mut c = 0u64;

            fn set_range(b: &mut Bitmap, c: &mut u64, i: usize, n: usize) {
                b.set_range(i, n);
                bits::set_bits_64(c, i, n);
                assert!(diff(&b, c) == None);
            }
            fn clear_range(b: &mut Bitmap, c: &mut u64, i: usize, n: usize) {
                b.clear_range(i, n);
                bits::clear_bits_64(c, i, n);
                assert!(diff(&b, c) == None);
            }
            fn shl(b: &mut Bitmap, c: &mut u64, n: usize) {
                b.shl(n);
                *c >>= n;
                assert!(diff(&b, c) == None);
            }
            fn shr(b: &mut Bitmap, c: &mut u64, n: usize) {
                b.shr(n);
                *c <<= n;
                assert!(diff(&b, c) == None);
            }

            shl(&mut b, &mut c, 5);
            shr(&mut b, &mut c, 6);
            set_range(&mut b, &mut c, 30, 10);
            shr(&mut b, &mut c, 1);
            shr(&mut b, &mut c, 8);
            clear_range(&mut b, &mut c, 40, 24);
            set_range(&mut b, &mut c, 50, 5);
            set_range(&mut b, &mut c, 58, 5);
            shl(&mut b, &mut c, 44);
            set_range(&mut b, &mut c, 6, 7);
            set_range(&mut b, &mut c, 16, 1);
            set_range(&mut b, &mut c, 19, 0);
            set_range(&mut b, &mut c, 25, 30);
            set_range(&mut b, &mut c, 63, 1);
            set_range(&mut b, &mut c, 62, 2);
            set_range(&mut b, &mut c, 0, 3);
            shl(&mut b, &mut c, 3);
            shl(&mut b, &mut c, 2);
            shl(&mut b, &mut c, 1);
            shl(&mut b, &mut c, 10);
            shl(&mut b, &mut c, 63);
        }
    }
}
