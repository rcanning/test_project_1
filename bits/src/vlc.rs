use super::{BitSrc, SliceBitSrc};

/// An entry in a variable-length code (VLC) table.
pub struct VLCEntry {
    /// 'n' bits, starting from the least significant. All unused bits must be 0.
    pub bits: u64,
    /// Number of bits in 'bits'
    pub n: u8,
    /// The output value
    pub value: u32,
}

/// Uses the given VLC table to parse one VLC-encoded value from 'bit_src'. vlc_table must be
/// in ascending order of 'n' (the code size). Returns None on failed lookup or no bits left
/// in 'bit_src'; the function consumes no input in these cases.
pub fn lookup_vlc(bit_src: &mut SliceBitSrc, vlc_table: &[VLCEntry]) -> Option<u32> {
    for num_bits in 1..65 {
        if bit_src.bits_remaining() < num_bits as usize {
            return None;
        }
        let bits = bit_src.peek(num_bits).unwrap();

        // A partial match means that the 'n' bits we extracted don't match any of
        // the codes in the VLC table, but they do match the first 'n' bits in at
        // least one table entry -- e.g. if 'n' is 3, the bits are 101, and the
        // VLC table contains:
        //     110
        //     1010
        //     1011
        // then we get a partial match on the second and third table entries.
        let mut got_partial_match = false;
        let mut prev_n = 0;
        for e in vlc_table {
            if num_bits == e.n && bits == e.bits {
                // Full match
                let _ = bit_src.extract(num_bits).unwrap();
                return Some(e.value);
            }
            if num_bits < e.n && bits == (e.bits >> (e.n - num_bits)) {
                got_partial_match = true;
                break;
            }
            if e.n < prev_n {
                panic!("VLC table is not in order");
            }
            prev_n = e.n
        }
        if !got_partial_match {
            break; // Not found
        }
    }
    return None;
}

#[cfg(test)]
mod tests {
    use super::super::SliceBitSrc;
    use super::*;

    // Converts a string of 1's and 0's to binary data. Ignores all other characters. Pads the
    // final byte with zeroes if s.len() % 8 != 0.
    fn get_test_data(s: &str) -> Vec<u8> {
        let approx_buf_size = (s.len() / 8) + 1;
        let mut b = Vec::with_capacity(approx_buf_size);

        let mut n = 0;
        for c in s.chars() {
            if c == '1' || c == '0' {
                let mut new_bit_value: u8 = if c == '1' { 1 } else { 0 };
                if n % 8 == 0 {
                    b.push(0u8);
                }
                new_bit_value <<= 7 - (n % 8);
                let cur_len = b.len();
                b[cur_len - 1] |= new_bit_value;
                n += 1;
            }
        }

        b
    }

    fn add_entry(tbl: &mut Vec<VLCEntry>, bits: u64, n: u8, value: u32) {
        tbl.push(VLCEntry {
            bits: bits,
            n: n,
            value: value,
        });
    }

    #[test]
    fn test_successful_lookups() {
        let mut test_table = Vec::new();
        add_entry(&mut test_table, 0b1, 1, 1);
        add_entry(&mut test_table, 0b010, 3, 2);
        add_entry(&mut test_table, 0b0110, 4, 3);
        add_entry(&mut test_table, 0b0111, 4, 4);

        let test_data = get_test_data("010 1 0111 1 0110 010");
        let mut x = SliceBitSrc::new(&test_data, 0);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 4);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 3);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert!(lookup_vlc(&mut x, &test_table).is_none());
    }

    #[test]
    fn test_unsucessful_lookup() {
        let mut test_table = Vec::new();
        add_entry(&mut test_table, 0b11, 2, 1);
        add_entry(&mut test_table, 0b010, 3, 2);
        add_entry(&mut test_table, 0b0110, 4, 3);
        add_entry(&mut test_table, 0b0111, 4, 4);

        let test_data = get_test_data("010 11 100");
        let mut x = SliceBitSrc::new(&test_data, 0);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert!(lookup_vlc(&mut x, &test_table).is_none());
    }

    #[test]
    fn test_with_real_data() {
        // Test with a real-life table
        // ISO 13818-2, Annex B, Table B.1
        let mut test_table = Vec::new();
        add_entry(&mut test_table, 0x1, 1, 1);
        add_entry(&mut test_table, 0x3, 3, 2);
        add_entry(&mut test_table, 0x2, 3, 3);
        add_entry(&mut test_table, 0x3, 4, 4);
        add_entry(&mut test_table, 0x2, 4, 5);
        add_entry(&mut test_table, 0x3, 5, 6);
        add_entry(&mut test_table, 0x2, 5, 7);
        add_entry(&mut test_table, 0x7, 7, 8);
        add_entry(&mut test_table, 0x6, 7, 9);
        add_entry(&mut test_table, 0xB, 8, 10);
        add_entry(&mut test_table, 0xA, 8, 11);
        add_entry(&mut test_table, 0x9, 8, 12);
        add_entry(&mut test_table, 0x8, 8, 13);
        add_entry(&mut test_table, 0x7, 8, 14);
        add_entry(&mut test_table, 0x6, 8, 15);
        add_entry(&mut test_table, 0x17, 10, 16);
        add_entry(&mut test_table, 0x16, 10, 17);
        add_entry(&mut test_table, 0x15, 10, 18);
        add_entry(&mut test_table, 0x14, 10, 19);
        add_entry(&mut test_table, 0x13, 10, 20);
        add_entry(&mut test_table, 0x12, 10, 21);
        add_entry(&mut test_table, 0x23, 11, 22);
        add_entry(&mut test_table, 0x22, 11, 23);
        add_entry(&mut test_table, 0x21, 11, 24);
        add_entry(&mut test_table, 0x20, 11, 25);
        add_entry(&mut test_table, 0x1F, 11, 26);
        add_entry(&mut test_table, 0x1E, 11, 27);
        add_entry(&mut test_table, 0x1D, 11, 28);
        add_entry(&mut test_table, 0x1C, 11, 29);
        add_entry(&mut test_table, 0x1B, 11, 30);
        add_entry(&mut test_table, 0x1A, 11, 31);
        add_entry(&mut test_table, 0x19, 11, 32);
        add_entry(&mut test_table, 0x18, 11, 33);

        let test_data = get_test_data("00000100010 1 00011 00011 1 1 011 00000011000");
        let mut x = SliceBitSrc::new(&test_data, 0);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 23);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 6);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 6);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 33);
    }

    #[test]
    fn test_vlc_boundary_cases() {
        // Test a mix of short and long codes
        let mut test_table = Vec::new();
        add_entry(&mut test_table, 0x1, 1, 1); // 1
        add_entry(&mut test_table, 0x1, 2, 2); // 01
        add_entry(&mut test_table, 0x1, 13, 3); // 0000000000001
        add_entry(&mut test_table, 0x1, 14, 4); // 00000000000001
        add_entry(&mut test_table, 0x0, 14, 5); // 00000000000000

        let test_data = get_test_data("0000000000001 01 1 01 00000000000000 00000000000001");
        let mut x = SliceBitSrc::new(&test_data, 0);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 3);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 5);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 4);

        // Test a VLC table with 64-bit codes
        let mut test_table = Vec::new();
        add_entry(&mut test_table, 0x0, 1, 1); // 0
        add_entry(&mut test_table, 0x2, 2, 2); // 10
        add_entry(
            &mut test_table,
            0b1110101010101010101010101010101010101010101010101010101010101010,
            64,
            3,
        );
        add_entry(
            &mut test_table,
            0b1111111111111111111111111011111111111111111111111111111111111111,
            64,
            4,
        );
        add_entry(
            &mut test_table,
            0b1111111111111111111111111111111111111111111011111111111111111111,
            64,
            5,
        );

        let test_data = get_test_data(
            "11111111111111111111111110111111 11111111111111111111111111111111 \
             10111111111111111111111111111111 11111111111110111111111111111111 \
             11011101010101010101010101010101 01010101010101010101010101010101 010",
        );
        let mut x = SliceBitSrc::new(&test_data, 0);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 4);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 2);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 5);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 1);
        assert_eq!(lookup_vlc(&mut x, &test_table).unwrap(), 3);
    }
}
