//! # Examples
//! ```
//!     use bits;
//!
//!     let mut u = 0u64;
//!     bits::bits::set_bit_64(&mut u, 0);
//!     assert_eq!(u, 1);
//!
//!     let mut u = 0u64;
//!     bits::bits::set_bits_64(&mut u, 3, 2);
//!     assert_eq!(u, 24); // u == 0x18, u == 0b00011000
//!
//!     let mut u = 0xFFFFFFFFFFFFFFFF;
//!     bits::bits::clear_bits_64(&mut u, 3, 2);
//!     assert_eq!(u, 0xFFFFFFFFFFFFFFE7);
//! ```

use std::mem::transmute;

/// Sets the 'ith' bit of 'u'.
pub fn set_bit_64(u: &mut u64, i: usize) {
    let mask = 1u64;
    *u |= mask << i
}

/// Clears the 'ith' bit of 'u'.
pub fn clear_bit_64(u: &mut u64, i: usize) {
    let mask = 1u64;
    *u &= !(mask << i)
}

/// Sets 'n' bits in 'u', starting from 'i'.
pub fn set_bits_64(u: &mut u64, i: usize, n: usize) {
    if n == 0 {
        // Need to handle this as a special case, or the line
        // "mask >>= 64 - n" will cause an integer overflow.
        return;
    }
    let mut mask = !0u64;
    mask >>= 64 - n;
    mask <<= i;
    *u |= mask;
}

/// Clears 'n' bits in 'u', starting from 'i'.
pub fn clear_bits_64(u: &mut u64, i: usize, n: usize) {
    if n == 0 {
        // Need to handle this as a special case, or the line
        // "mask >>= 64 - n" will cause an integer overflow.
        return;
    }
    let mut mask = !0u64;
    mask >>= 64 - n;
    mask <<= i;
    *u &= !mask;
}

/// Returns true if bit 'i' of 'u' is set.
pub fn is_set_64(u: &u64, i: usize) -> bool {
    let mask = 1u64;
    (u & (mask << i)) != 0
}

/// Returns true if bit 'i' of 'b' is set.
pub fn is_set_8(b: &u8, i: usize) -> bool {
    let mask = 1u8;
    (b & (mask << i)) != 0
}

/// Removes all '1's from 'n' except the least significant one.
///    E.g., lsb_mask(0b00110100) returns 0b00000100
/// Currently doesn't support the MS bit being set, as this triggers an overflow
/// exception in Rust. lsb_mask(0) triggers an assertion.
pub fn lsb_mask(n: u64) -> u64 {
    assert!(n != 0);
    assert_eq!(n & 0x8000_0000_0000_0000, 0);
    n & unsafe { transmute::<i64, u64>(-(transmute::<u64, i64>(n))) }
}

/// Returns index (0-63) of most significant bit set. Returns 0 if no
/// bit is set.
pub fn ms_bit_set(mut n: u64) -> u8 {
    if n == 0 {
        return 0;
    }

    let mut b: u8 = 1;

    fn step(num_bits: u8, n: &mut u64, b: &mut u8) {
        if *n >= (1u64 << num_bits) {
            *b += num_bits;
            *n >>= num_bits;
        }
    }

    step(32, &mut n, &mut b);
    step(16, &mut n, &mut b);
    step(8, &mut n, &mut b);
    step(4, &mut n, &mut b);
    step(2, &mut n, &mut b);
    step(1, &mut n, &mut b);
    b - 1
}

// ================================================================================================
// Adds each octal digit in 'n' (that is, each set of three bits, treated as an
// octal value). Note: Will overflow if the sum is greater than 510. Not for
// general use.
// ================================================================================================
fn add_octal_digits(n: u64) -> u64 {
    // In binary:
    // 1000000111000000111000000111000000111000000111000000111000000111
    const EVERY_THIRD_OCTAL_DIGIT: u64 = 0x81C0E070381C0E07;

    let sh0: u64 = n & EVERY_THIRD_OCTAL_DIGIT;
    let sh3: u64 = (n >> 3) & EVERY_THIRD_OCTAL_DIGIT;
    let sh6: u64 = (n >> 6) & EVERY_THIRD_OCTAL_DIGIT;
    let sum: u64 = sh0 + sh3 + sh6;

    // 'sum' now contains 8 values: seven values of 9 bits, and the most significant
    // bit is a value by itself. To get the required sum, we need to add these values
    // up. We can take advantage of the fact that the value is effectively in base 512:
    // to add up the values, just mod by 511. Note that this will fail if the sum
    // is greater than 510 (see above), but that cannot happen, since it must
    // obviously be less than 64.
    return sum % 511;
}

// ================================================================================================
// ================================================================================================
pub fn count_bits(n: u64) -> u64 {
    // The function uses a clever algorithm developed at MIT, and nicely explained by
    // Gurmeet Singh Manku at:
    // http://gurmeetsingh.wordpress.com/2008/08/05/fast-bit-counting-routines/
    //
    // Here's my attempt at explaining it:
    //
    // A 3-bit number (abc) can be thought of as:
    //       4a + 2b + c
    // Right-shift the number by 1, and you get 2a + b. Right-shift again, and you get a.
    // It so happens that:
    //       4a + 2b + c
    //          - 2a + b
    //               - a
    //       = a + b + c,
    // which happens to be the value we're after. So for values up to 3 bits,
    // (x - (x >> 1) - (x >> 2)) gives the answer. For larger values, we can
    // generalize the algorithm as follows:

    // 1011011011011011011011011011011011011011011011011011011011011011
    const SKIP_EVERY_THIRD_DIGIT: u64 = 0xB6DB6DB6DB6DB6DB;

    // 1001001001001001001001001001001001001001001001001001001001001001
    const EVERY_THIRD_DIGIT: u64 = 0x9249249249249249;

    // Right-shift by 1 and mask out every third digit (the ones that were
    // incorrectly shifted from one octal digit to the adjacent one).
    let sh1: u64 = (n >> 1) & SKIP_EVERY_THIRD_DIGIT;

    // Right-shift by 2 and mask out two of every three digits (the ones
    // that were incorrectly shifted from one octal digit to the adjacent
    // one).
    let sh2: u64 = (n >> 2) & EVERY_THIRD_DIGIT;

    let tmp: u64 = n - sh1 - sh2;

    // 'tmp' now contains 22 octal values, which when added give the number
    // of bits.
    return add_octal_digits(tmp);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_clear_bit() {
        let mut x: u64 = 0xff;

        clear_bit_64(&mut x, 6);
        assert!(x == 0xbf);
        clear_bit_64(&mut x, 5);
        assert!(x == 0x9f);

        set_bit_64(&mut x, 12);
        assert!(x == 0x109f);
        clear_bit_64(&mut x, 12);
        assert!(x == 0x9f);
    }

    #[test]
    fn test_clear_bits() {
        let mut x: u64 = 0xFF;
        clear_bits_64(&mut x, 1, 2);
        assert!(x == 0xF9);
        clear_bits_64(&mut x, 6, 1);
        assert!(x == 0xB9);

        let mut x = 0xFFFF;
        clear_bits_64(&mut x, 4, 4);
        clear_bits_64(&mut x, 6, 3);
        assert!(x == 0xFE0F);

        let mut x = 0xFFFFFFFF;
        clear_bits_64(&mut x, 0, 32);
        assert!(x == 0);

        let mut x = 0xFFFFFFFF;
        clear_bits_64(&mut x, 0, 31);
        assert!(x == 0x80000000);

        let mut x = 0xFFFFFFFF;
        clear_bits_64(&mut x, 1, 31);
        assert!(x == 1);

        clear_bits_64(&mut x, 19, 0);
        assert!(x == 1);
    }

    #[test]
    fn test_set_bit() {
        let mut x: u64 = 0;
        set_bit_64(&mut x, 0);
        assert!(x == 0x00000001);
        set_bit_64(&mut x, 4);
        set_bit_64(&mut x, 5);
        assert!(x == 0x31);

        set_bit_64(&mut x, 9);
        assert!(x == 0x231);
        set_bit_64(&mut x, 5);
        assert!(x == 0x231);

        set_bit_64(&mut x, 16);
        assert!(x == 0x10231);
        set_bit_64(&mut x, 17);
        assert!(x == 0x30231);
        set_bit_64(&mut x, 31);
        assert!(x == 0x80030231);

        set_bit_64(&mut x, 32);
        assert!(x == 0x180030231);
        set_bit_64(&mut x, 61);
        set_bit_64(&mut x, 62);
        assert!(x == 0x6000000180030231);
    }

    #[test]
    fn test_set_bits() {
        let mut x: u64 = 0;
        set_bits_64(&mut x, 0, 2);
        assert!(x == 0x00000003);
        set_bits_64(&mut x, 6, 1);
        assert!(x == 0x43);

        x = 0;
        set_bits_64(&mut x, 4, 4);
        set_bits_64(&mut x, 6, 3);
        assert!(x == 0x1F0);

        x = 0;
        set_bits_64(&mut x, 0, 32);
        assert!(x == 0xFFFFFFFF);

        x = 0;
        set_bits_64(&mut x, 0, 31);
        assert!(x == 0x7FFFFFFF);

        x = 0;
        set_bits_64(&mut x, 1, 31);
        assert!(x == 0xFFFFFFFE);

        x = 0;
        set_bits_64(&mut x, 5, 50);
        assert!(x == 0x007FFFFFFFFFFFE0);

        set_bits_64(&mut x, 19, 0);
        assert!(x == 0x007FFFFFFFFFFFE0);
    }

    #[test]
    fn test_is_set() {
        let mut x: u64 = 0;
        assert!(!is_set_64(&x, 0));
        assert!(!is_set_64(&x, 1));
        assert!(!is_set_64(&x, 7));

        set_bit_64(&mut x, 0);
        assert!(is_set_64(&x, 0));
        assert!(!is_set_64(&x, 1));
        assert!(!is_set_64(&x, 7));

        set_bit_64(&mut x, 12);
        assert!(is_set_64(&x, 0));
        assert!(!is_set_64(&x, 1));
        assert!(!is_set_64(&x, 7));
        assert!(!is_set_64(&x, 11));
        assert!(is_set_64(&x, 12));
        assert!(!is_set_64(&x, 13));
    }

    #[test]
    fn test_lsb_mask() {
        // Test single bits in various positions
        assert_eq!(lsb_mask(0x1), 0x1);
        assert_eq!(lsb_mask(0x2), 0x2);
        assert_eq!(lsb_mask(0x80), 0x80);
        assert_eq!(lsb_mask(0x100), 0x100);
        assert_eq!(lsb_mask(0x8000), 0x8000);
        assert_eq!(lsb_mask(0x4000_0000_0000_0000), 0x4000_0000_0000_0000);
        // NOT SUPPORTED YET assert_eq!(lsb_mask(0x8000_0000_0000_0000), 0x8000_0000_0000_0000);

        assert_eq!(lsb_mask(0x1A), 0x2);
        assert_eq!(lsb_mask(0xFF), 0x1);
        assert_eq!(lsb_mask(0x1FE), 0x2);
        assert_eq!(lsb_mask(0x4FFF_FFFF_FFFF_FFFF), 0x1);
        assert_eq!(lsb_mask(0x4FFF_FFFF_FFFF_FF00), 0x100);
        assert_eq!(lsb_mask(0x4FFF_FFAA_FFFF_FE00), 0x200);
        assert_eq!(lsb_mask(0x4FFF_FFFF_FAAF_FC00), 0x400);
        assert_eq!(lsb_mask(0x4FFF_AAAA_AAAA_F800), 0x800);
    }

    #[test]
    fn test_ms_bit_set() {
        // Verify the first 1000 values
        for i in 1..1000 {
            let pos = ms_bit_set(i);

            let mut test_val: u64 = 1;
            test_val <<= pos;
            let mut b = i;
            b >>= pos;
            b <<= pos;
            assert_eq!(b, test_val);
        }

        // Verify some selected values
        assert_eq!(ms_bit_set(0), 0);
        assert_eq!(ms_bit_set(1), 0);

        assert_eq!(ms_bit_set(0xFFFF), 15);
        assert_eq!(ms_bit_set(0xF001), 15);
        assert_eq!(ms_bit_set(0x8001), 15);
        assert_eq!(ms_bit_set(0x8000), 15);
        assert_eq!(ms_bit_set(0x8180), 15);

        assert_eq!(ms_bit_set(0x10000000), 28);
        assert_eq!(ms_bit_set(0x20000000), 29);
        assert_eq!(ms_bit_set(0x40000000), 30);
        assert_eq!(ms_bit_set(0x80000000), 31);
        assert_eq!(ms_bit_set(0xA0000000), 31);

        // Try a bit in every possible position
        for i in 0..64 {
            let val = 1u64 << i;
            assert_eq!(ms_bit_set(val), i);
        }

        // Try a pair of bits in every possible position
        for i in 0..63 {
            let val = 0x3u64 << i;
            assert_eq!(ms_bit_set(val), i + 1);
        }
    }

    #[test]
    fn test_count_bits() {
        // Test all zero
        assert_eq!(count_bits(0x0000000000000000), 0);

        // Test all ones
        assert_eq!(count_bits(0xFFFFFFFFFFFFFFFF), 64);

        // Test a 1 in every position
        for i in 0..64 {
            let mut x: u64 = 0;
            set_bit_64(&mut x, i);
            assert_eq!(count_bits(x), 1);
        }

        // Test a 0 in every position
        for i in 0..64 {
            let mut x: u64 = 0xFFFFFFFFFFFFFFFF;
            clear_bit_64(&mut x, i);
            assert_eq!(count_bits(x), 63);
        }

        // Test 1 at each end
        assert_eq!(count_bits(0x8000000000000001), 2);

        // Test a few random values
        assert_eq!(count_bits(0xA0000807002050FF), 17);
        assert_eq!(count_bits(0x0A000807002050F8), 14);
        assert_eq!(count_bits(0x00000000000000FF), 8);
        assert_eq!(count_bits(0xFF00000000000000), 8);
        assert_eq!(count_bits(0x8421842184218421), 16);
        assert_eq!(count_bits(0x0102040810204080), 8);
        assert_eq!(count_bits(0x017E00E070070E02), 20);
    }
}
