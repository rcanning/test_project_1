use byteorder;
use byteorder::ByteOrder;

use super::BitSrc;

use std::cmp::min;

/// # SliceBitSrc
/// Implements BitSrc using a [u8] as the source. The order of extraction
/// is from the MSb of the first byte in the slice to the LSb of the last.
///
/// If extract() fails, it consumes no bits, so subsequent calls (with a
/// smaller 'n') may succeed.
/// # Examples
/// ```
/// use bits::{SliceBitSrc, BitSrc};
/// let bytes = [0xAE, 0x0A]; // 1010 1110 0000 1010
/// let mut src = SliceBitSrc::new(&bytes[..], 0);
/// let mut ex = (&mut src) as &mut BitSrc;
/// assert_eq!(ex.extract_bit().unwrap(), true);
/// assert_eq!(ex.extract_bit().unwrap(), false);
/// assert_eq!(ex.extract(12).unwrap(), 0xB82);
/// ```

/* ================================================================================================
Implementation Notes
====================

I've tried replacing the range-checked slice accesses with unsafe, unchecked
ones, but it made no difference to performance.
================================================================================================ */

pub struct SliceBitSrc<'a> {
    // Contains the next 'num_bits' bits.
    cur_word: u64,
    // Number of bits remaining to extract from 'cur_word'.
    num_bits: u8,
    // Remaining bytes from which to extract bits after 'cur_word' is exhausted.
    data: &'a [u8],
}

const BITS_PER_BYTE: u8 = 8;
const BYTES_PER_WORD64: u64 = 8;
const BITS_PER_WORD64: u8 = 64;

// ================================================================================================

impl<'a> SliceBitSrc<'a> {
    /// # Arguments
    /// * `data` - Byte array to extract bits from.
    /// * `bit_offset` - Position (counting from MSb) of the first bit to extract from
    ///   data[0]
    ///
    /// `data` may be empty only if `bit_offset` is zero.
    pub fn new(data: &[u8], bit_offset: u8) -> SliceBitSrc {
        if bit_offset == 0 {
            SliceBitSrc {
                cur_word: 0,
                num_bits: 0,
                data: data,
            }
        } else {
            SliceBitSrc {
                cur_word: data[0] as u64,
                num_bits: BITS_PER_BYTE - bit_offset,
                data: &data[1..],
            }
        }
    }

    /// Returns number of bits remaining to be extracted.
    #[inline(always)]
    pub fn bits_remaining(&self) -> usize {
        self.num_bits as usize + (BITS_PER_BYTE as usize * self.data.len()) as usize
    }

    /// Returns true if the extractor is currently on a byte boundary.
    #[inline(always)]
    pub fn is_byte_aligned(&self) -> bool {
        self.num_bits % BITS_PER_BYTE == 0
    }

    /// Same as extract(), but doesn't advance the internal read pointer.
    pub fn peek(&self, n: u8) -> Option<u64> {
        let mut tmp = SliceBitSrc {
            cur_word: self.cur_word,
            data: self.data,
            num_bits: self.num_bits,
        };
        tmp.extract(n)
    }
}

// ================================================================================================

impl<'a> BitSrc for SliceBitSrc<'a> {
    fn extract(&mut self, mut n: u8) -> Option<u64> {
        // Returns true if the first byte of 'b' is on a 64-bit boundary.
        fn is_aligned_64(b: &[u8]) -> bool {
            use std::mem::transmute;
            let n: u64 = unsafe { transmute(b.as_ptr()) };
            n % BYTES_PER_WORD64 == 0
        }

        // ========================================================================================

        if n as usize > self.bits_remaining() {
            return None;
        }

        let mut u: u64 = 0;
        while n > 0 {
            if self.num_bits == 0 {
                if self.data.len() >= 8 && is_aligned_64(&self.data) {
                    // Load 64 bits in one go. I've found this gives a performance boost of
                    // about 25% over loading one byte at a time.
                    self.cur_word = byteorder::BigEndian::read_u64(&self.data[..8]);
                    self.num_bits = BITS_PER_WORD64;
                    self.data = &self.data[8..];
                } else {
                    // Load 8 bits
                    self.cur_word = self.data[0] as u64;
                    self.num_bits = BITS_PER_BYTE;
                    self.data = &self.data[1..];
                }
            }

            // Calculate how many bits to copy on this iteration
            let num_bits_to_copy = min(n, self.num_bits);

            // Make room in 'u'
            u = if num_bits_to_copy == BITS_PER_WORD64 {
                0
            } else {
                u << num_bits_to_copy
            };

            let bits = {
                let mut mask: u64 = 0xFFFFFFFFFFFFFFFF;
                mask >>= BITS_PER_WORD64 - self.num_bits;
                (self.cur_word & mask) >> (self.num_bits - num_bits_to_copy)
            };

            u |= bits;
            n -= num_bits_to_copy;
            self.num_bits -= num_bits_to_copy;
        }
        Some(u)
    }

    fn num_unaligned_bits(&self) -> u8 {
        self.num_bits
    }
}

// ================================================================================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic() {
        // Simple bit-by-bit test
        let bytes = [0xAE, 0xAA, 0xA0, 0xAA];
        let mut ex = SliceBitSrc::new(&bytes[..], 0);
        let res = "10101110101010101010000010101010".to_string();
        let mut iter = res.as_bytes().iter();
        while let Some(expected) = iter.next() {
            assert!((*expected == '1' as u8) == ex.extract_bit().unwrap());
        }
    }

    #[test]
    fn test_long_extraction() {
        let b = [0xA7, 0x55, 0xFF, 0xAA, 0xDD, 0xDD, 0xDD, 0x77];
        let mut ex = SliceBitSrc::new(&b[..], 0);
        let ul = ex.extract(60).unwrap();
        assert!(ul == 0x0A755FFAADDDDDD7);
    }

    #[test]
    fn test_boundary_cases() {
        let b = [0xA7, 0x55, 0xFF, 0xAA, 0xDD, 0xDD, 0xDD, 0x77];
        let mut ex = SliceBitSrc::new(&b[..], 0);
        assert!(ex.extract_bit().unwrap());
        assert!(ex.extract(4).unwrap() == 0x4);

        // Test an extraction that crosses a byte boundary
        assert!(ex.extract(6).unwrap() == 0x3A);

        // Test an extraction that crosses multiple byte boundaries
        assert!(ex.extract(17).unwrap() == 0x15FFA);

        // Test an extraction that starts on a byte boundary
        assert!(!ex.is_byte_aligned());
        assert!(ex.extract(4).unwrap() == 0xA);
        assert!(ex.is_byte_aligned());
        assert!(ex.extract(12).unwrap() == 0xDDD);

        // Test an extraction that ends on a byte boundary, and that
        // further extractions succeed
        assert!(ex.extract(12).unwrap() == 0xDDD);
        assert!(ex.extract(7).unwrap() == 0x3B);
        assert!(ex.extract(1).unwrap() == 0x1);

        // The SliceBitSrc is now exhausted; is_byte_aligned() is defined
        // to return true in this case.
        assert!(ex.is_byte_aligned());
    }

    #[test]
    // Tests the bits_remaining() function
    fn test_bits_remaining() {
        let b = [0xA7, 0x55, 0xAA];
        let mut ex = SliceBitSrc::new(&b[..], 0);
        assert!(ex.bits_remaining() == 24);

        ex.extract(5).unwrap();
        assert!(ex.bits_remaining() == 19);

        ex.extract(15).unwrap();
        assert!(ex.bits_remaining() == 4);

        ex.extract(3).unwrap();
        assert!(ex.bits_remaining() == 1);

        ex.extract(1).unwrap();
        assert!(ex.bits_remaining() == 0);

        ex.extract(0).unwrap();
        assert!(ex.bits_remaining() == 0);
    }

    #[test]
    fn test_part_byte() {
        // Test an extractor that starts from part-way through a byte
        let b = [0xA7, 0x55, 0xFF, 0xAA];
        let mut ex = SliceBitSrc::new(&b[..], 4);
        assert!(ex.extract(8).unwrap() == 0x75);
    }

    #[test]
    fn test_peek() {
        // Tests the peek() function.
        let b = [0xA7, 0x55, 0xFF, 0xAA];
        let mut ex = SliceBitSrc::new(&b[..], 0);
        assert!(ex.extract(4).unwrap() == 0xA);
        assert!(ex.peek(4).unwrap() == 0x7);
        assert!(ex.peek(4).unwrap() == 0x7);
        assert!(ex.extract(4).unwrap() == 0x7);
        assert!(ex.extract(8).unwrap() == 0x55);
        assert!(ex.peek(8).unwrap() == 0xFF);
        assert!(ex.peek(4).unwrap() == 0xF);
        assert!(ex.peek(2).unwrap() == 0x3);
        assert!(ex.extract(8).unwrap() == 0xFF);
    }

    #[test]
    fn test_failed_extract() {
        let b = [0xA7, 0x56];
        let mut ex = SliceBitSrc::new(&b[..], 0);
        assert!(ex.extract(8).unwrap() == 0xA7);
        assert!(ex.extract(9).is_none());
        assert!(ex.extract(4).unwrap() == 0x05);

        // Test a failed peek()
        assert!(ex.peek(4).unwrap() == 0x06);
        assert!(ex.peek(5).is_none());
        assert!(ex.peek(4).unwrap() == 0x06);

        // Check that a failed extract() consumes no bits.
        assert!(ex.extract(5).is_none());
        assert!(ex.extract(4).unwrap() == 0x06);
    }
}
