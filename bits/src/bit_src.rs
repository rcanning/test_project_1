// ================================================================================================
/// # BitSrc
/// BitSrc extracts bits from a data source, in order, and in groups of
/// any size up to 64.
// ================================================================================================
pub trait BitSrc {
    // ============================================================================================
    /// Extract up to 64 bits at a time. If n < 64, the bits will be placed at the LS
    /// end of the return value - e.g. extract(1) will return a u64 with the value
    /// of 0 or 1.
    ///
    /// Returns None if less than 'n' bits were available. In this case, no bits
    /// are consumed.
    // ============================================================================================
    fn extract(&mut self, n: u8) -> Option<u64>;

    // ============================================================================================
    /// Returns number of bits (0-7) that must be extracted to make the source
    /// byte-aligned (e.g. returns 0 if already byte-aligned).
    // ============================================================================================
    fn num_unaligned_bits(&self) -> u8;

    // ============================================================================================
    // ============================================================================================
    #[inline(always)]
    fn extract_bit(&mut self) -> Option<bool> {
        self.extract(1).map(|b| b != 0)
    }
}
