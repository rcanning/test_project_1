// ================================================================================================
//! Decodes signed and unsigned integers encoded using Exponential Golomb coding.
//! [Reference](https://pythonhosted.org/bitstring/exp-golomb.html)
//!
//! # Examples
//! ```
//! use bits::exp_golomb::decode_unsigned;
//! use bits::exp_golomb::decode_signed;
//! use bits::SliceBitSrc;
//! let bits = [0b00001001, 0b10110110, 0b10110001, 0b00100101];
//! let mut xtract = SliceBitSrc::new(&bits, 2);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 3);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 0);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 0);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 2);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 2);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 1);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 0);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 0);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 8);
//! assert_eq!(decode_unsigned(&mut xtract).unwrap(), 4);
//! let bits = [0b00101001, 0b10010000, 0b10100110, 0b00111000,
//!             0b10000001, 0b00100010, 0b10000101, 0b10001100];
//! let mut xtract = SliceBitSrc::new(&bits, 2);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 0);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 1);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), -1);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 2);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), -2);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 3);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), -3);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 4);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), -4);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 5);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), -5);
//! assert_eq!(decode_signed(&mut xtract).unwrap(), 6);
//! ```
// ================================================================================================

use super::bits;
use super::BitSrc;

// ================================================================================================
/// Returns Err(number of bits consumed) if no valid Golomb coding was found.
// ================================================================================================
pub fn decode_unsigned(xtract: &mut dyn BitSrc) -> Result<u64, usize> {
    let mut bit_count: u8 = 0;
    loop {
        let bit = match xtract.extract_bit() {
            None => return Err(bit_count as usize),
            Some(b) => b,
        };
        if bit {
            break;
        }
        bit_count += 1;
    }
    let mut bits = match xtract.extract(bit_count) {
        None => return Err(bit_count as usize),
        Some(b) => b,
    };
    bits::set_bit_64(&mut bits, bit_count as usize);
    Ok(bits - 1)
}

// ================================================================================================
/// Returns Err(number of bits consumed) if no valid Golomb coding was found.
// ================================================================================================
pub fn decode_signed(xtract: &mut dyn BitSrc) -> Result<i64, usize> {
    let unsigned = decode_unsigned(xtract)?;
    Ok(if unsigned % 2 == 0 {
        -((unsigned / 2) as i64)
    } else {
        ((unsigned / 2) + 1) as i64
    })
}
