use std::cmp::min;

use crate::bit_src::BitSrc;
use crate::defs::BITS_PER_BYTE;

/// # IterBitSrc
/// Implements BitSrc using a byte iterator.
/// # Examples
/// ```
/// use bits::{BitSrc, IterBitSrc};
/// let bytes = [0xAE, 0x0A]; // 1010 1110 0000 1010
/// let mut src = IterBitSrc::new(bytes.iter().map(|b| *b), 0);
/// let mut ex = (&mut src) as &mut BitSrc;
/// assert_eq!(ex.extract_bit().unwrap(), true);
/// assert_eq!(ex.extract_bit().unwrap(), false);
/// assert_eq!(ex.extract(12).unwrap(), 0xB82);
/// ```

// ================================================================================================

pub struct IterBitSrc<I>
where
    I: Iterator<Item = u8>,
{
    // Source of bytes
    src: I,
    cur_byte: u8,
    // Number of bits remaining to extract from 'cur_byte'.
    num_bits: u8,
}

// ================================================================================================

impl<I> IterBitSrc<I>
where
    I: Iterator<Item = u8>,
{
    /// # Arguments
    /// * `src` - source of bytes.
    /// * `bit_offset` - Position (counting from MSb) of the first bit to extract from
    /// the first byte
    /// The iteration range may be empty only if `bit_offset` is zero.
    pub fn new(mut src: I, bit_offset: u8) -> IterBitSrc<I> {
        if bit_offset == 0 {
            IterBitSrc {
                src: src,
                cur_byte: 0,
                num_bits: 0,
            }
        } else {
            let b = src.next().unwrap();
            IterBitSrc {
                src: src,
                cur_byte: b,
                num_bits: BITS_PER_BYTE - bit_offset,
            }
        }
    }
}

// ================================================================================================

impl<I> BitSrc for IterBitSrc<I>
where
    I: Iterator<Item = u8>,
{
    fn extract(&mut self, mut n: u8) -> Option<u64> {
        let mut u: u64 = 0;
        while n > 0 {
            if self.num_bits == 0 {
                self.cur_byte = match self.src.next() {
                    None => return None,
                    Some(b) => b,
                };
                self.num_bits = BITS_PER_BYTE;
            }

            let num_bits_to_copy = min(n, self.num_bits);

            // Make room in 'u'
            u <<= num_bits_to_copy;

            let bits = {
                let mut mask: u8 = 0xFF;
                mask >>= BITS_PER_BYTE - self.num_bits;
                (self.cur_byte & mask) >> (self.num_bits - num_bits_to_copy)
            };

            u |= bits as u64;
            n -= num_bits_to_copy;
            self.num_bits -= num_bits_to_copy;
        }
        Some(u)
    }

    fn num_unaligned_bits(&self) -> u8 {
        self.num_bits
    }
}

// ================================================================================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic() {
        // Simple bit-by-bit test
        let bytes = [0xAE, 0xAA, 0xA0, 0xAA];
        let mut ex = IterBitSrc::new(bytes.iter().map(|b| *b), 0);
        let res = "10101110101010101010000010101010".to_string();
        let mut iter = res.as_bytes().iter();
        while let Some(expected) = iter.next() {
            assert!((*expected == '1' as u8) == ex.extract_bit().unwrap());
        }
        assert!(ex.extract_bit().is_none());
    }

    #[test]
    fn test_long_extraction() {
        let b = [0xA7, 0x55, 0xFF, 0xAA, 0xDD, 0xDD, 0xDD, 0x77];
        let mut ex = IterBitSrc::new(b.iter().map(|b| *b), 0);
        let ul = ex.extract(60).unwrap();
        assert!(ul == 0x0A755FFAADDDDDD7);
    }

    #[test]
    fn test_boundary_cases() {
        let b = [0xA7, 0x55, 0xFF, 0xAA, 0xDD, 0xDD, 0xDD, 0x77];
        let mut ex = IterBitSrc::new(b.iter().map(|b| *b), 0);
        assert!(ex.extract_bit().unwrap());
        assert!(ex.extract(4).unwrap() == 0x4);

        // Test an extraction that crosses a byte boundary
        assert!(ex.extract(6).unwrap() == 0x3A);

        // Test an extraction that crosses multiple byte boundaries
        assert!(ex.extract(17).unwrap() == 0x15FFA);

        // Test an extraction that starts on a byte boundary
        assert!(ex.extract(4).unwrap() == 0xA);
        assert!(ex.extract(12).unwrap() == 0xDDD);

        // Test an extraction that ends on a byte boundary, and that
        // further extractions succeed
        assert!(ex.extract(12).unwrap() == 0xDDD);
        assert!(ex.extract(7).unwrap() == 0x3B);
        assert!(ex.extract(1).unwrap() == 0x1);

        assert!(ex.extract_bit().is_none());
    }

    #[test]
    fn test_part_byte() {
        // Test an extractor that starts from part-way through a byte
        let b = [0xA7, 0x55, 0xFF, 0xAA];
        let mut ex = IterBitSrc::new(b.iter().map(|b| *b), 4);
        assert!(ex.extract(8).unwrap() == 0x75);
    }

    #[test]
    fn test_real_data_1() {
        let b = [0x01, 0x01, 0x60, 0x00, 0x00, 0x00, 0xB0];
        let mut ex = IterBitSrc::new(b.iter().map(|b| *b), 0);
        assert_eq!(ex.extract(4).unwrap(), 0);
        assert_eq!(ex.extract(3).unwrap(), 0);
        assert!(ex.extract_bit().unwrap());

        assert_eq!(ex.extract(2).unwrap(), 0);
        assert!(!ex.extract_bit().unwrap());
        assert_eq!(ex.extract(5).unwrap(), 1);
        assert_eq!(ex.extract(32).unwrap(), 0x60000000);
    }
}
